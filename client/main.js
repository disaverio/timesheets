import angular from 'angular';
import angularMeteor from 'angular-meteor';

import { Meteor } from 'meteor/meteor';

import customers from '../imports/components/customers/customers';
import projects from '../imports/components/projects/projects';
import timesheets from '../imports/components/timesheets/timesheets';
import insights from '../imports/components/insights/insights';
import '../imports/startup/accounts-config.js';

angular.module('freelance-manager', [angularMeteor, customers.name, projects.name, timesheets.name, insights.name, 'accounts.ui']);