import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Timesheets = new Mongo.Collection('timesheets');

if (Meteor.isServer) {
    Meteor.publish('timesheets', function() {
        return Timesheets.find({
            createdBy: this.userId
        });
    });
}

Meteor.methods({
    'timesheets.removeAll' () {
        Timesheets.remove({});
    },

    'timesheets.insert': insert,

    'timesheets.remove': remove,

    'timesheets.update' (timesheetId, hours) {
        check(timesheetId, String);
        check(hours, Number);

        const timesheet = Timesheets.findOne(timesheetId);
        if (timesheet.createdBy !== Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        remove(timesheetId);

        insert(timesheet.day, timesheet.project, hours);
    },
});

function insert(day, projectId, hours) {
    check(day, Number);
    check(projectId, String);
    check(hours, Number);

    if (!Meteor.userId()) {
        throw new Meteor.Error('not-authorized');
    }

    var now = new Date();

    Timesheets.insert({
        day: day,
        project: projectId,
        hours: hours,
        createdAt: now,
        lastUpdate: now,
        createdBy: Meteor.userId()
    });
}

function remove(timesheetId) {
    check(timesheetId, String);

    const timesheet = Timesheets.findOne(timesheetId);
    if (timesheet.createdBy !== Meteor.userId()) {
        throw new Meteor.Error('not-authorized');
    }

    Timesheets.remove(timesheetId);
}