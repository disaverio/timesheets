import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Timesheets } from './timesheets.js';

export const Projects = new Mongo.Collection('projects');

if (Meteor.isServer) {
    Meteor.publish('projects', function() {
        return Projects.find({
            createdBy: this.userId
        });
    });
}

Meteor.methods({
    'projects.insert' (project) {
        check(project, Object);

        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        var now = new Date();

        Projects.insert({
            name: project.name || '',
            customerId: project.customerId || '',
            hourlyRate: project.hourlyRate || 0,
            dueDate: project.dueDate || -1,
            note: project.note || '',
            completed: project.completed == true,
            createdAt: now,
            lastUpdate: now,
            createdBy: Meteor.userId()
        });
    },

    'projects.remove' (projectId) {
        check(projectId, String);

        const project = Projects.findOne(projectId);
        if (project.createdBy !== Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        var relatedTimesheets = Timesheets.find({
            project: projectId
        });

        relatedTimesheets.forEach(function(timesheet) {
            Meteor.call('timesheets.remove', timesheet._id);
        });

        Projects.remove(projectId);
    },

    'projects.update' (projectId, project) {
        check(projectId, String);
        check(project, Object);

        const projectToUpdate = Projects.findOne(projectId);
        if (projectToUpdate.createdBy !== Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        Projects.update(projectId, {
            $set: {
                name: project.customerId == undefined ? projectToUpdate.name : project.name,
                customerId: project.customerId == undefined ? projectToUpdate.customerId : project.customerId,
                hourlyRate: project.hourlyRate == undefined ? projectToUpdate.hourlyRate : project.hourlyRate,
                dueDate: project.dueDate == undefined ? projectToUpdate.dueDate : project.dueDate,
                note: project.note == undefined ? projectToUpdate.note : project.note,
                completed: project.completed == undefined ? projectToUpdate.completed : project.completed == true,
                lastUpdate: new Date()
            }
        });
    }
});