import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Projects } from './projects.js';

export const Customers = new Mongo.Collection('customers');

if (Meteor.isServer) {
    Meteor.publish('customers', function() {
        return Customers.find({
            createdBy: this.userId
        });
    });
}

Meteor.methods({
    'customers.insert' (name, VAT, email) {
        check(name, String);
        check(VAT, String);

        if (!Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        var now = new Date();

        Customers.insert({
            name: name,
            vat: VAT,
            email: email || '',
            createdAt: now,
            lastUpdate: now,
            createdBy: Meteor.userId()
        });
    },

    'customers.remove' (customerId) {
        check(customerId, String);

        const customer = Customers.findOne(customerId);
        if (customer.createdBy !== Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        var relatedProjects = Projects.find({
            customerId: customerId
        });

        relatedProjects.forEach(function(project) {
            Meteor.call('projects.remove', project._id);
        });

        Customers.remove(customerId);
    },

    'customers.update' (customerId, properties) {
        check(customerId, String);
        check(properties, Object);

        const customer = Customers.findOne(customerId);
        if (customer.createdBy !== Meteor.userId()) {
            throw new Meteor.Error('not-authorized');
        }

        Customers.update(customerId, {
            $set: {
                name: properties.name || customer.name,
                vat: properties.vat || customer.vat,
                email: properties.email || customer.email || '',
                lastUpdate: new Date()
            }
        });
    }
});