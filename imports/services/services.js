import angular from 'angular';
import angularMeteor from 'angular-meteor';
import '../../node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js';


class Utils {

    constructor($uibModal) {
        this.uibModal = $uibModal;
    }

    launchModal(controller, template, params, dimension) {
        return this.uibModal.open({
            animation: true,
            size: dimension|| 'lg',
            resolve: { params: function() { return params } },
            templateUrl: template,
            controller: ('modalController', ['$uibModalInstance', '$scope', 'params', controller])
        });
    }

    getUTCTime(date) {
        return Date.UTC(1900 + date.getYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0);
    }
}


export default angular.module('services', [angularMeteor, 'ui.bootstrap'])
    .service('utils', ['$uibModal', Utils]);