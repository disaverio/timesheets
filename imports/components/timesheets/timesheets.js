import angular from 'angular';
import angularMeteor from 'angular-meteor';

import { Meteor } from 'meteor/meteor';
import { Timesheets } from '../../api/timesheets.js';
import { Projects } from '../../api/projects.js';
import { Customers } from '../../api/customers.js';

import '../../services/services.js';
import timesheetsTemplate from './timesheets.html';


class TimesheetsCtrl {

    constructor($scope, utils) {
        // Meteor.call('timesheets.removeAll');
        $scope.viewModel(this);

        this.subscribe('timesheets');
        this.subscribe('projects');

        this.utils = utils;
        this.dateInput = new Date();

        this.helpers({
            timesheets() {
                return Timesheets.find({
                    $and: [{
                        day: {
                            $gte: getWeekDays.call(this, this.getReactively('dateInput'))[0]
                        }
                    }, {
                        day: {
                            $lte: getWeekDays.call(this, this.getReactively('dateInput'))[6]
                        }
                    }]
                }, {
                    sort: { day: 1 }
                });
            },
            projects() {
                var projects = Projects.find({}, {
                    sort: { dueDate: 1 }
                });

                return projects.map(function(project) {
                    project.customerName = Customers.findOne(project.customerId).name;
                    return project;
                });
            }
        });

        $scope.days = [];
        $scope.form = {};
        $scope.totals = {};
        $scope.hideCompleted = true;

        $scope.$watch('$ctrl.projects', (function(newValue, oldValue) {
            $scope.totals = getTotals(this.projects, this.timesheets, $scope.hideCompleted);
        }).bind(this), true);

        $scope.$watch('$ctrl.timesheets', (function(newValue, oldValue) {
            $scope.form = getForm(this.timesheets);
            $scope.totals = getTotals(this.projects, this.timesheets, $scope.hideCompleted);
        }).bind(this), true);

        $scope.$watch('$ctrl.dateInput', (function(newValue, oldValue) {
            newValue.setHours(0,0,0,0);
            $scope.days = getWeekDays.call(this, newValue);
        }).bind(this));

        $scope.$watch('hideCompleted', (function(newValue, oldValue) {
            $scope.totals = getTotals(this.projects, this.timesheets, $scope.hideCompleted);
        }).bind(this));
    }

    save(projectId, day, timesheet) {
        if (!timesheet) {
            return;
        } else if (!timesheet.hours && timesheet.id) {
            Meteor.call('timesheets.remove', timesheet.id);
        } else if (timesheet.hours > 0 && timesheet.id) {
            Meteor.call('timesheets.update', timesheet.id, timesheet.hours);
        } else if (timesheet.hours > 0 && !timesheet.id) {
            Meteor.call('timesheets.insert', day, projectId, timesheet.hours);
        }
    }
}

/*
 * Given a date, returns array of UTC dates of the week
 * @param {Date} date
 * @return {Array} of UTC dates
 */
function getWeekDays(date) {
    var weekDays = [];
    var dayOfWeek = new Date(date);

    for (var i=0; i<7; i++) {
        dayOfWeek.setDate(dayOfWeek.getDate() - dayOfWeek.getDay() + i);
        weekDays.push(this.utils.getUTCTime(dayOfWeek));
    }

    return weekDays;
}

/*
 * Set form of timesheets table
 * @param {Array} timesheets: array of existing timesheets
 * @return {Object} form with values
 */
function getForm(timesheets) {

    var form = {};

    timesheets.forEach(function(timesheet) {
        form[timesheet.project] = form[timesheet.project] || {};
        form[timesheet.project][timesheet.day] = {
            id:  timesheet._id,
            hours: timesheet.hours
        }
    });

    return form;
}

/*
 * Set total values of table
 * @param {Array} projects: array of all projects
 * @param {Array} timesheets: array of existing timesheets
 * @param {Boolean} hideCompleted: to exclude completed project from computation
 * #return {Object} with total values
 */
function getTotals(projects, timesheets, hideCompleted) {

    var totals = {};

    if (hideCompleted) {
        var completedProjectsMap = getCompletedProjectsMap();
    }

    totals.projects = {};
    totals.days = {};
    totals.total = 0;

    timesheets.forEach(function(timesheet) {
        var workedHours = hideCompleted && completedProjectsMap[timesheet.project] ? 0 : timesheet.hours;

        totals.projects[timesheet.project] = totals.projects[timesheet.project] || 0;
        totals.projects[timesheet.project] += workedHours;

        totals.days[timesheet.day] = totals.days[timesheet.day] || 0;
        totals.days[timesheet.day] += workedHours;

        totals.total += workedHours;
    });

    return totals;

    // create support structure to access data in constant time
    function getCompletedProjectsMap() {
        var map = {};
        projects.forEach(function(project) {
            map[project._id] = project.completed;
        });
        return map;
    }
}


export default angular.module('timesheets', [angularMeteor, 'services'])
    .component('timesheets', {
        templateUrl: timesheetsTemplate,
        controller: ['$scope', 'utils', TimesheetsCtrl]
    });