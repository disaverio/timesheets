import angular from 'angular';
import angularMeteor from 'angular-meteor';

import { Meteor } from 'meteor/meteor';
import { Customers } from '../../api/customers.js';

import '../../services/services.js';
import customersTemplate from './customers.html';
import editCustomerTemplate from './edit-customer.html';


class CustomersCtrl {

    constructor($scope, utils) {
        $scope.viewModel(this);

        this.subscribe('customers');

        this.utils = utils;

        this.helpers({
            customers() {
                return Customers.find({}, {
                    sort: { name: 1 }
                });
            }
        })
    }

    addCustomer() {
        var modalInstance = this.utils.launchModal(editCustomer, editCustomerTemplate, { title: 'Add new customer' });

        modalInstance.result.then(function(closingParam) {
            Meteor.call('customers.insert', closingParam.name, closingParam.vat, closingParam.email);
        }, function(dismissingParam) {

        });
    }

    removeCustomer(customer) {
        Meteor.call('customers.remove', customer._id);
    }

    editCustomer(customer) {
        var modalInstance = this.utils.launchModal(editCustomer, editCustomerTemplate, { customer: customer, title: 'Edit customer' });

        modalInstance.result.then(function(closingParam) {
            Meteor.call('customers.update', customer._id, closingParam);
        }, function(dismissingParam) {

        });
    }
}

/*
 * Controller for edit modal
 */
function editCustomer($uibModalInstance, $scope, params) {

    params = params || {};
    params.customer = params.customer || {};

    $scope.title = params.title || 'Customers';

    $scope.form = {
        name: params.customer.name,
        vat: params.customer.vat,
        email: params.customer.email
    };

    $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.save = function () {
        $uibModalInstance.close({
            name: $scope.form.name,
            vat: $scope.form.vat,
            email: $scope.form.email
        });
    };
}


export default angular.module('customers', [angularMeteor, 'services'])
    .component('customers', {
        templateUrl: customersTemplate,
        controller: ['$scope', 'utils', CustomersCtrl]
    });