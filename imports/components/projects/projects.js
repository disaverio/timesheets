import angular from 'angular';
import angularMeteor from 'angular-meteor';

import { Meteor } from 'meteor/meteor';
import { Projects } from '../../api/projects.js';
import { Customers } from '../../api/customers.js';

import '../../services/services.js';
import projectsTemplate from './projects.html';
import editProjectTemplate from './edit-project.html';


class ProjectsCtrl {

    constructor($scope, utils) {
        $scope.viewModel(this);

        this.subscribe('projects');
        this.subscribe('customers');

        this.utils = utils;

        this.helpers({
            projects() {
                var projects = Projects.find({}, {
                    sort: { dueDate: 1 }
                });

                return projects.map(function(project) {
                    project.customerName = Customers.findOne(project.customerId).name;
                    return project;
                });
            },
            customers() {
                return Customers.find({}, {
                    sort: { name: 1 }
                });
            }
        });

        $scope.hideCompleted = false;
        $scope.today = this.utils.getUTCTime(new Date());
    }

    addProject() {
        var modalInstance = this.utils.launchModal(editProject, editProjectTemplate, { customers: this.customers, title: 'Add new project' });

        modalInstance.result.then(function(closingParam) {
            Meteor.call('projects.insert', closingParam);
        });
    }

    removeProject(project) {
        Meteor.call('projects.remove', project._id);
    }

    editProject(project) {
        var modalInstance = this.utils.launchModal(editProject, editProjectTemplate, { customers: this.customers, project: project, title: 'Edit project' });

        modalInstance.result.then(function(closingParam) {
            Meteor.call('projects.update', project._id, closingParam);
        });
    }
}

/*
 * Controller for edit modal
 */
function editProject($uibModalInstance, $scope, params) {

    params = params || {};

    $scope.title = params.title || 'Projects';
    $scope.customers = params.customers || [];
    $scope.project = angular.copy(params.project || {});

    $scope.close = $uibModalInstance.dismiss.bind(null, 'cancel');

    $scope.save = $uibModalInstance.close.bind(null, $scope.project);
}


export default angular.module('projects', [angularMeteor, 'services'])
    .component('projects', {
        templateUrl: projectsTemplate,
        controller: ['$scope', 'utils', ProjectsCtrl]
    });