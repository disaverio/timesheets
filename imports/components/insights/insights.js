import angular from 'angular';
import angularMeteor from 'angular-meteor';

import { Meteor } from 'meteor/meteor';
import { Timesheets } from '../../api/timesheets.js';
import { Projects } from '../../api/projects.js';
import { Customers } from '../../api/customers.js';

import '../../services/services.js';
import '../../filters/filters.js';
import insightsTemplate from './insights.html';

var Highcharts = require('highcharts');
require('highcharts/modules/exporting')(Highcharts);

var charts;
var colors;
var colorMap;


class InsightsCtrl {

    constructor($scope, utils) {
        $scope.viewModel(this);

        this.subscribe('timesheets');
        this.subscribe('projects');
        this.subscribe('customers');

        this.utils = utils;
        this.toDateInput = new Date();
        this.fromDateInput = (function() {
            var oneMonthAgo = new Date();
            oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 1);
            return oneMonthAgo;
        })();

        this.helpers({
            timesheets() {
                return Timesheets.find({
                    $and: [{
                        day: {
                            $gte: this.utils.getUTCTime(this.getReactively('fromDateInput'))
                        }
                    }, {
                        day: {
                            $lte: this.utils.getUTCTime(this.getReactively('toDateInput'))
                        }
                    }]
                }, {
                    sort: { day: 1 }
                });
            },
            projects() {
                return Projects.find({}, {
                    sort: { dueDate: 1 }
                });
            },
            customers() {
                return Customers.find({}, {
                    sort: { name: 1 }
                });
            }
        });

        $scope.fromDateOptions = { maxDate: this.toDateInput };
        $scope.toDateOptions = { minDate: this.fromDateInput };

        $scope.$watch('$ctrl.timesheets', (function(newValue, oldValue) {
            var response = getTotals(this.customers, this.projects, this.timesheets, this.toDateInput, this.fromDateInput);
            $scope.customers = response.customers;
            $scope.projects = response.projects;
            $scope.showInsights = response.valuesPresent;
        }).bind(this), true);

        $scope.$watch('$ctrl.projects', (function(newValue, oldValue) {
            var response = getTotals(this.customers, this.projects, this.timesheets, this.toDateInput, this.fromDateInput);
            $scope.customers = response.customers;
            $scope.projects = response.projects;
            $scope.showInsights = response.valuesPresent;
        }).bind(this), true);

        $scope.$watch('$ctrl.customers', (function(newValue, oldValue) {
            var response = getTotals(this.customers, this.projects, this.timesheets, this.toDateInput, this.fromDateInput);
            $scope.customers = response.customers;
            $scope.projects = response.projects;
            $scope.showInsights = response.valuesPresent;
        }).bind(this), true);

        $scope.$watch('$ctrl.fromDateInput', (function(newValue, oldValue) {
            $scope.toDateOptions.minDate = this.fromDateInput;
        }).bind(this));

        $scope.$watch('$ctrl.toDateInput', (function(newValue, oldValue) {
            $scope.fromDateOptions.maxDate = this.toDateInput;
        }).bind(this));

        charts = {};
        colors = Highcharts.getOptions().colors;
        colorMap = {};
    }
}

/*
 * Function to get color for a given customer
 * @param {Object} customer, from Collection
 * @return {String} with hex color
 */
function getColor(customer) {
    colorMap[customer._id] = colorMap[customer._id] || colors[Object.keys(colorMap).length % colors.length];
    return colorMap[customer._id];
}

/*
 * Function for charts creation
 * @param {Array} chartsDescriptors, array of Object, with chart details
 * @param {Array} customers, array of Object, from Collection
 * @param {Array} projects, array of Object, from Collection
 */
function createCharts(chartsDescriptors, customers, projects) {

    chartsDescriptors.forEach(function (chart) {
        createChart(chart.attributes, chart.htmlElement, chart.property, customers, projects);
    });

    function createChart(attributes, htmlElement, property, customers, projects) {

        customers.sort(descSortingFn);
        projects.sort(descSortingFn);

        var dataSet = getDataSet(property, customers, projects);

        drawChart(attributes, htmlElement, dataSet);

        function getDataSet(property, customers, projects) {

            var customersData = [];
            var projectsData = [];

            customers.forEach(function (customer) {
                customersData.push({
                    name: customer.name,
                    y: customer[property],
                    color: customer.color
                });
                var projForCustomer = projects.filter(function (project) {
                    return project.customerId == customer._id;
                });
                projForCustomer.forEach(function (project, pIndex) {
                    var brightness = 0.15 + (pIndex / projForCustomer.length) / 5;
                    projectsData.push({
                        name: project.name,
                        y: project[property],
                        color: Highcharts.Color(customer.color).brighten(brightness).get()
                    });
                });
            });

            return {
                customersData: customersData,
                projectsData: projectsData
            };
        }

        function drawChart(attributes, htmlElement, dataSet) {

            if (!charts[htmlElement]) {
                charts[htmlElement] = Highcharts.chart(htmlElement, {
                    chart: {type: 'pie'},
                    title: {text: attributes.title},
                    subtitle: {text: attributes.subtitle},
                    exporting: false,
                    credits: false,
                    plotOptions: {
                        pie: {
                            shadow: false,
                            center: ['50%', '50%']
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<b>{point.name}</b>: {point.y}' + attributes.valueSuffix,
                    },
                    series: [{
                        showInLegend: true,
                        name: 'Customers',
                        size: '60%',
                        dataLabels: false
                    }, {
                        name: 'Projects',
                        size: '80%',
                        innerSize: '60%',
                        dataLabels: {
                            formatter: function () {
                                return this.y > 0 ? '<b>' + this.point.name + ':</b> ' + this.y + attributes.valueSuffix : null;
                            }
                        }
                    }]
                });
            }

            charts[htmlElement].series[0].setData(dataSet.customersData);
            charts[htmlElement].series[1].setData(dataSet.projectsData);
        }

        function descSortingFn(a, b) {
            return -1 * (a[property] - b[property]);
        }
    }
}

/*
 * Function for totals computation
 * @param {Array} customers, array of Object, from Collection
 * @param {Array} projects, array of Object, from Collection
 * @param {Array} timesheets, array of Object, from Collection
 * @param {Date} to, starting date
 * @param {Date} from, ending date
 */
function getTotals(customers, projects, timesheets, to, from) {

    customers = angular.copy(customers);
    projects = angular.copy(projects);

    var valuesPresent = false;

    projects.forEach(function(project) {
        var timesheetsForProject = timesheets.filter(function(timesheet) {
            return timesheet.project == project._id;
        });
        var workedHours = timesheetsForProject.reduce(function(accumulator, currValue) {
            return accumulator + currValue.hours;
        }, 0);
        var income = workedHours * project.hourlyRate;
        project.workedHours = workedHours;
        project.income = income;
        if (workedHours > 0) {
            valuesPresent = true;
        }
    });

    customers.forEach(function(customer, index) {
        var projectsForCustomer = projects.filter(function(project) {
            return project.customerId == customer._id;
        });
        var workedHours = projectsForCustomer.reduce(function(accumulator, currValue) {
            return accumulator + currValue.workedHours;
        }, 0);
        var income = projectsForCustomer.reduce(function(accumulator, currValue) {
            return accumulator + currValue.income;
        }, 0);
        customer.workedHours = workedHours;
        customer.income = income;
        customer.color = getColor(customer);
    });

    createCharts([{
        attributes: {
            title: 'Worked hours',
            subtitle: 'For the period ' + from.toLocaleDateString() + " - " + to.toLocaleDateString(),
            valueSuffix: 'h'
        },
        htmlElement: 'hoursChartContainer',
        property: 'workedHours'
    }, {
        attributes: {
            title: 'Incomes',
            subtitle: 'For the period ' + from.toLocaleDateString() + " - " + to.toLocaleDateString(),
            valueSuffix: '€'
        },
        htmlElement: 'incomesChartContainer',
        property: 'income'
    }], customers, projects);

    return {
        customers: customers,
        projects: projects,
        valuesPresent: valuesPresent
    }
}


export default angular.module('insights', [angularMeteor, 'customFilters', 'services'])
    .component('insights', {
        templateUrl: insightsTemplate,
        controller: ['$scope', 'utils', InsightsCtrl]
    });