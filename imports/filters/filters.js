import angular from 'angular';
import angularMeteor from 'angular-meteor';


export default angular.module('customFilters', [angularMeteor])
    .filter('filterByProperty', filterByProperty);

/*
 * Given an array, filter it by property, value and operator
 */
function filterByProperty() {
    return function (itemsArray, property, operator, value) {

        return itemsArray.filter(function(item) {

            switch(operator) {
                case '===': return item[property] === value;
                case '!==': return item[property] !== value;
                case '==': return item[property] == value;
                case '!=': return item[property] != value;
                case '>=': return item[property] >= value;
                case '<=': return item[property] <= value;
                case '>': return item[property] > value;
                case '<': return item[property] < value;
            }
        });
    };
}